# Automation tests for qlcick-api-tests
It's test project for check ability to writing high quality automated acceptance tests

# Versions
Maven version: Apache Maven 3.3.9
Java version: 8

# Screenplay Pattern with Serenity BDD and Cucumber

In the project we will look at how to use the Screenplay Pattern with Serenity BDD

## Screenplay implementation

The project structure below:
````
+ context
    Java configuration classes of spring context
+ step_definitions
    Step Definition is a java method in a class with an annotation above it
+ features
    Test scenarions in gherkin style
````
## Get the code
git clone https://gitlab.com/itkochov/qlcick-api-tests.git

## Running the project
To run the project you'll need JDK 1.8 and Maven installed.

## Application Under Test
Give ability to get product details via api
* URL : https://waarkoop-server.herokuapp.com/

### Test Execution
```
mvn clean verify
```
To run test cases on gitlab:
- create push to repo
- gitlab-ci.yml file in project allows to run pipeline automatically after each push to repo

## Viewing the reports
- This will produce a Serenity test report in the `target/site/serenity` directory (open `index.html`)
- XML report will be available in GitLab under 'Test' tab in pipeline.
- Serenity dynamics reports will be available in GitLab under 'Settings' -> 'Pages' -> 'Your pages are served under'
  `https://gitlab.com/itkochov/qlcick-api-tests/pages`
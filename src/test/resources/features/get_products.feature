Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

### According of the task description I can't modify the Cucumber scenarios
### Such scenario include positive and negative cases
### Check of Available products: "apple", "mango", "tofu", "water" it's a positive cases because we have requirements for it
### For "cars" we don't have any requirements, so it's a negative cases.

  Scenario: User should have ability to get data for available products
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
    Then he sees the results displayed for tofu
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/water"
    Then he sees the results displayed for water

  Scenario: User should not be able to see details for not available product
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he doesn not see the results

### If I could change scenario I'll write it in such manner
###  I prefer to use some common description like in scenario below. Some common variables for test scenarios will be stored in configuration

###  Positive case
#  Scenario: User should have ability to get data for available products
#    Given I'm as a user
#    When I try to get data for:
#    |apple|mango|tofu|water|
#    Then I should see successful results

### Negative case
#  Scenario: User should not be able to see details for not available product
#    Given I'm as a user
#    When I try to get data for car
#    And Result should not be found

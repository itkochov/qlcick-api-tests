import com.context.ApplicationContext;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

@RunWith(CucumberWithSerenity.class)
@ContextConfiguration(classes = ApplicationContext.class)
@CucumberOptions(
        glue = {"com.step_definitions"},
        features = {"classpath:features"}
)
public class TestRunner {
}

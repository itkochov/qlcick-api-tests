package com.step_definitions;

import com.context.SpringContextAware;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;

public class StepDefinitions implements SpringContextAware {

    @Autowired
    private Actor user;

    @When("^he calls endpoint \"(.*)\"$")
    public void he_calls_endpoint(String url) {
        user.whoCan(CallAnApi.at(url));
        user.attemptsTo(Get.resource(""));
    }

    @Then("^he sees the results displayed for (.*)$")
    public void he_sees_the_results_displayed_for_apple(String product) {
        user.should(
                seeThatResponse(String.format("all the expected %ss should be returned", product),
                        response -> response.statusCode(HttpStatus.SC_OK)
                                .body("size()", is(not(0)))
                                .body("provider", everyItem(is(not(empty()))))
                                .body("title", everyItem(is(not(empty()))))
                                .body("url", everyItem(is(not(empty()))))
                                .body("brand", everyItem(is(not(empty()))))
                                .body("price", everyItem(is(not(empty()))))
                                .body("unit", everyItem(is(not(empty()))))
                                .body("isPromo", everyItem(is(not(empty()))))
                                .body("promoDetails", everyItem(is(not(empty()))))
                                .body("image", everyItem(is(not(empty())))))
        );
    }

    @Then("^he doesn not see the results$")
    public void he_doesn_not_see_the_results() {
        user.should(
                seeThatResponse(
                        response -> response.statusCode(HttpStatus.SC_NOT_FOUND)
                                .body("detail.error", is(true))
                                .body("detail.message", is("Not found"))
                                .body("detail.requested_item", is("car"))
                                .body("detail.served_by", is("https://waarkoop.com")))
        );
    }
}

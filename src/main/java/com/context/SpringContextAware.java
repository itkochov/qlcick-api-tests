package com.context;

import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = ApplicationContext.class)
public interface SpringContextAware {
}
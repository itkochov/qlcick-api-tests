package com.context;

import net.serenitybdd.screenplay.Actor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com")
@PropertySource("classpath:application.properties")
public class ApplicationContext {

    @Bean
    public Actor getActor() {
        return Actor.named("User");
    }
}
